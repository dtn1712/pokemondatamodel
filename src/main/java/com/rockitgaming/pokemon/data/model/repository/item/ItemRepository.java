package com.rockitgaming.pokemon.data.model.repository.item;

import com.rockitgaming.pokemon.data.model.entity.item.Item;
import com.rockitgaming.pokemon.data.model.entity.item.ItemCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {

    Item findByName(String name);

    List<Item> findByCategory(ItemCategory category);

    List<Item> findByCostGreaterThan(Long cost);

    List<Item> findByCostGreaterThanEqual(Long cost);

    List<Item> findByCostLessThan(Long cost);

    List<Item> findByCostLessThanEqual(Long cost);
}
