package com.rockitgaming.pokemon.data.model.entity.player;

/**
 * Created by dangn on 8/14/16.
 */
public enum PlayerStatusEnum {

    ACTIVE, LOCKED
}
