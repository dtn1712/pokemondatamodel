package com.rockitgaming.pokemon.data.model.entity.player;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.BaseEntity;
import com.rockitgaming.pokemon.data.model.entity.pokemon.PokemonSpecies;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = TableName.PLAYER_POKEDEX_TABLE)
public class PlayerPokedex extends BaseEntity{

    public static final String GET_PLAYER_POKEDEX_CACHE_KEY = "getPlayerPokedex";

    @ManyToOne
    @JoinColumn(name = "playerId")
    private Player player;

    @ManyToOne
    @JoinColumn(name = "pokemonSpeciesId")
    private PokemonSpecies pokemonSpecies;

    @Column(name = "isOwned")
    private boolean isOwned;
}
