package com.rockitgaming.pokemon.data.model.entity.pokemon;


public enum ShapeEnum {

    Ball, Squiggle, Fish, Arms, Blob, Upright, Legs, Quadruped, Wings, Tentacles, Heads, Humanoid, Bug_Wings, Armor

}
