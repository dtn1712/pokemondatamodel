package com.rockitgaming.pokemon.data.model.etc;

public enum RequestType {
    BATTLE, RESPONSE, TRADE
}
