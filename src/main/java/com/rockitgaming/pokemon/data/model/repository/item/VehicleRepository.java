package com.rockitgaming.pokemon.data.model.repository.item;

import com.rockitgaming.pokemon.data.model.entity.item.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

    List<Vehicle> findByName(String name);

    List<Vehicle> findBySpeed(int speed);

    List<Vehicle> findBySpeedGreaterThan(int speed);

    List<Vehicle> findBySpeedGreaterThanEqual(int speed);

    List<Vehicle> findBySpeedLessThan(int speed);

    List<Vehicle> findBySpeedLessThanEqual(int speed);
}
