package com.rockitgaming.pokemon.data.model.repository.player;


import com.rockitgaming.pokemon.data.model.entity.player.Player;
import com.rockitgaming.pokemon.data.model.entity.player.PlayerParty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerPartyRepository extends JpaRepository<PlayerParty, Long> {

    PlayerParty findByPlayer(Player player);
}
