package com.rockitgaming.pokemon.data.model.entity.pokemon;

import com.rockitgaming.pokemon.data.model.entity.BaseEntity;
import lombok.Data;

@Data
public class PokemonTypeEffect extends BaseEntity{

    public enum EffectType { ATTACKING, DEFENDING }

    private EffectType effectType;
    private double effectRate;
    private PokemonType fromPokemonType;
    private PokemonType toPokemonType;
}
