package com.rockitgaming.pokemon.data.model.entity.account;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE)
public class AccountAttemptLogin extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "accountId", nullable = false)
    private Account account;

    @Column(name="isLoginSuccess")
    private boolean isLoginSuccess;

    @Column(name = "loginTime", nullable = false, updatable = false, insertable = false, columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP")
    private DateTime loginTime;

    @Column(name = "loginIP")
    private String loginIP;

    @Column(name = "isSessionLogout", nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
    private boolean isSessionLogout;
}
