package com.rockitgaming.pokemon.data.model.repository.player;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.account.Account;
import com.rockitgaming.pokemon.data.model.entity.player.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {

    Player findByName(String name);

    List<Player> findByAccount(Account account);

    @Query(value = "SELECT CASE WHEN COUNT(*) > 0 THEN 'true' ELSE 'false' END FROM "
            + TableName.PLAYER_TABLE + " p WHERE p.name = :name", nativeQuery = true)
    Boolean existsByName(@Param("name") String name);
}
