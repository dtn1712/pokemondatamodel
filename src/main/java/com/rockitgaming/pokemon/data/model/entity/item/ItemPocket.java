package com.rockitgaming.pokemon.data.model.entity.item;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = TableName.ITEM_POCKET_TABLE)
public class ItemPocket extends BaseEntity {

    @Column(name = "key")
    private String key;

    @Column(name = "name")
    private String name;
}
