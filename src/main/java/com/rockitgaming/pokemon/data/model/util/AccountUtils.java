package com.rockitgaming.pokemon.data.model.util;


import com.rockitgaming.pokemon.data.model.entity.account.Account;
import com.rockitgaming.pokemon.data.model.entity.player.Player;

public class AccountUtils {

    public static Account getSecureAccount(Account rawAccount) {
        if (rawAccount == null) {
            return null;
        }

        Account secureAccount = new Account();
        secureAccount.setId(rawAccount.getId());
        secureAccount.setPhone(rawAccount.getPhone());
        secureAccount.setUsername(rawAccount.getUsername());
        secureAccount.setCreatedAt(rawAccount.getCreatedAt());
        secureAccount.setAccountType(rawAccount.getAccountType());
        secureAccount.setAccountStatus(rawAccount.getAccountStatus());
        secureAccount.setCurrentPlayer(rawAccount.getCurrentPlayer());
        secureAccount.setLastSuccessLoginTime(rawAccount.getLastSuccessLoginTime());

        if (secureAccount.getCurrentPlayer() != null) {
            Player currentPlayer = secureAccount.getCurrentPlayer();
            currentPlayer.setAccount(null);
            secureAccount.setCurrentPlayer(currentPlayer);
        }
        return secureAccount;
    }
}
