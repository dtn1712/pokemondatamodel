package com.rockitgaming.pokemon.data.model.entity.player;

/**
 * Created by dangn on 8/13/16.
 */
public enum FriendRequestStatusEnum {

    Pending, Accept, Ignore
}
