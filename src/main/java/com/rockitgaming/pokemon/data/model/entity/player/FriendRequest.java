package com.rockitgaming.pokemon.data.model.entity.player;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = TableName.FRIEND_REQUEST_TABLE)
public class FriendRequest extends BaseEntity{

    public FriendRequest(Player fromPlayer, Player toPlayer) {
        this.fromPlayer = fromPlayer;
        this.toPlayer = toPlayer;
        this.status = FriendRequestStatusEnum.Pending;
    }

    @Enumerated(EnumType.STRING)
    @Column(name="requestStatus")
    private FriendRequestStatusEnum status;

    @Column(name = "createdAt", nullable = false, updatable = false, insertable = false, columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP")
    private Date sendTime;

    @ManyToOne
    @JoinColumn(name="fromPlayerId")
    private Player fromPlayer;

    @ManyToOne
    @JoinColumn(name="toPlayerId")
    private Player toPlayer;
}
