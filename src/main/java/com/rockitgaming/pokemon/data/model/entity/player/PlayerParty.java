package com.rockitgaming.pokemon.data.model.entity.player;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.BaseEntity;
import com.rockitgaming.pokemon.data.model.entity.pokemon.Pokemon;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = TableName.PLAYER_PARTY_TABLE)
public class PlayerParty extends BaseEntity{

    public static final String GET_PLAYER_PARTY_CACHE_KEY = "getPlayerParty";

    @ManyToOne
    @JoinColumn(name = "playerId")
    private Player player;

    @ManyToOne
    @JoinColumn(name = "pokemon1Id")
    private Pokemon pokemon1;

    @ManyToOne
    @JoinColumn(name = "pokemon2Id")
    private Pokemon pokemon2;

    @ManyToOne
    @JoinColumn(name = "pokemon3Id")
    private Pokemon pokemon3;

    @ManyToOne
    @JoinColumn(name = "pokemon4Id")
    private Pokemon pokemon4;

    @ManyToOne
    @JoinColumn(name = "pokemon5Id")
    private Pokemon pokemon5;

    @ManyToOne
    @JoinColumn(name = "pokemon6Id")
    private Pokemon pokemon6;
}
