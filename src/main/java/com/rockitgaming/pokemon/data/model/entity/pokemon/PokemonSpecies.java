package com.rockitgaming.pokemon.data.model.entity.pokemon;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = TableName.POKEMON_SPECIES_TABLE)
public class PokemonSpecies extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "height")
    private double height;

    @Column(name = "weight")
    private double weight;

    @Enumerated(EnumType.STRING)
    @Column(name = "color")
    private ColorEnum color;

    @Enumerated(EnumType.STRING)
    @Column(name = "shape")
    private ShapeEnum shape;

    @Enumerated(EnumType.STRING)
    @Column(name = "habitat")
    private HabitatEnum habitat;

    @Column(name = "genderRate")
    private int genderRate;

    @Column(name = "captureRate")
    private int captureRate;

    @Column(name = "baseHappiness")
    private int baseHappiness;

    @Column(name = "isBaby")
    private boolean isBaby;

    @Column(name = "isGenderDifference")
    private boolean isGenderDifference;

    @Column(name = "hatchCounter")
    private int hatchCounter;

    @OneToOne
    @JoinColumn(name = "evolveFromSpeciesId")
    private PokemonSpecies evolveFromSpecies;

    @Column(name = "growthRate")
    private GrowthRateEnum growthRate;

    @ManyToOne
    @JoinColumn(name = "pokemonType1Id")
    private PokemonType pokemonType1;

    @ManyToOne
    @JoinColumn(name = "pokemonType2Id")
    private PokemonType pokemonType2;
}


