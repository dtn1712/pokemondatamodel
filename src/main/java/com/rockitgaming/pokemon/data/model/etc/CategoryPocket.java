package com.rockitgaming.pokemon.data.model.etc;

public enum CategoryPocket {

    Misc, PokeBalls, Medicine, Machines, Berries, Mail, Battle, Key
}
