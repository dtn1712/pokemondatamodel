package com.rockitgaming.pokemon.data.model.repository.player;

import com.rockitgaming.pokemon.data.model.entity.player.Player;
import com.rockitgaming.pokemon.data.model.entity.player.PlayerParty;
import com.rockitgaming.pokemon.data.model.entity.player.PlayerPokedex;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerPokedexRepository extends JpaRepository<PlayerPokedex, Long> {

    PlayerPokedex findByPlayer(Player player);
}
