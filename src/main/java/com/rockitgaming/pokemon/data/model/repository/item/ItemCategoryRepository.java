package com.rockitgaming.pokemon.data.model.repository.item;

import com.rockitgaming.pokemon.data.model.entity.item.ItemCategory;
import com.rockitgaming.pokemon.data.model.entity.item.ItemPocket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemCategoryRepository extends JpaRepository<ItemCategory, Long> {

    ItemCategory findByKey(String key);

    List<ItemCategory> findByPocket(ItemPocket pocket);
}
