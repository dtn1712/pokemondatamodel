package com.rockitgaming.pokemon.data.model.entity.account;

public enum AccountStatusEnum {
    ACTIVE,
    BANNED,
    INACTIVE,
    LOCKED
}
