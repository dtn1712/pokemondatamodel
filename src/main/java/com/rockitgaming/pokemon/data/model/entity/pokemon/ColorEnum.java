package com.rockitgaming.pokemon.data.model.entity.pokemon;

/**
 * Created by dangn on 7/23/16.
 */
public enum ColorEnum {

    Black, Blue, Brown, Gray, Green, Pink, Purple, Red, White, Yellow

}
