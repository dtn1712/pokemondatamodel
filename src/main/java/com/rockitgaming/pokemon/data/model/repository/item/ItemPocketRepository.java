package com.rockitgaming.pokemon.data.model.repository.item;

import com.rockitgaming.pokemon.data.model.entity.item.ItemPocket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemPocketRepository extends JpaRepository<ItemPocket, Long> {

    ItemPocket findByKey(String key);
}
