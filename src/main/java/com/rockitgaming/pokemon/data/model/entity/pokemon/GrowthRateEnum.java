package com.rockitgaming.pokemon.data.model.entity.pokemon;


public enum GrowthRateEnum {

    Slow, Medium, Fast, Medium_Slow, Slow_Then_Very_Fast, Fast_Then_Very_Slow

}
