package com.rockitgaming.pokemon.data.model.entity.player;

public enum LanguageEnum {
    ENGLISH, VIETNAMESE
}
