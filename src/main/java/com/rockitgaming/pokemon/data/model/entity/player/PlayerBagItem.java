package com.rockitgaming.pokemon.data.model.entity.player;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.BaseEntity;
import com.rockitgaming.pokemon.data.model.entity.item.Item;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = TableName.PLAYER_BAG_ITEM_TABLE)
public class PlayerBagItem extends BaseEntity{

    public static final String GET_PLAYER_BAG_ITEMS_CACHE_KEY = "getPlayerBagItems";

    @Column(name = "quantity")
    private int quantity;

    @ManyToOne
    @JoinColumn(name = "itemId")
    private Item item;

    @ManyToOne
    @JoinColumn(name = "playerId")
    private Player player;
}
