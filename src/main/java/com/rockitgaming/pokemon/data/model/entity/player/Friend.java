package com.rockitgaming.pokemon.data.model.entity.player;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = TableName.FRIEND_TABLE)
public class Friend extends BaseEntity {

    public Friend(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    @Column(name = "friendTime", nullable = false, updatable = false, insertable = false, columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP")
    private DateTime friendTime;

    @ManyToOne
    @JoinColumn(name = "player1Id")
    private Player player1;

    @ManyToOne
    @JoinColumn(name = "player2Id")
    private Player player2;
}
