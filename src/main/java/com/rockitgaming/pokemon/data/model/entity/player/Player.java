package com.rockitgaming.pokemon.data.model.entity.player;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.BaseEntity;
import com.rockitgaming.pokemon.data.model.entity.account.Account;
import com.rockitgaming.pokemon.data.model.entity.item.Item;
import com.rockitgaming.pokemon.data.model.entity.item.Vehicle;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = TableName.PLAYER_TABLE)
public class Player extends BaseEntity {

    public static final String GET_PLAYER_CACHE_KEY = "getPlayer";
    public static final String GET_CURRENT_PLAYER_CACHE_KEY = "getCurrentPlayer";
    public static final String GET_PLAYERS_BY_ACCOUNT_CACHE_KEY = "getPlayersByAccount";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "accountId", nullable = false)
    private Account account;

    @Column(name = "name")
    private String name;

    @Column(name = "speed")
    private int speed;

    @Column(name = "money")
    private Long money;

    @Column(name = "sprite")
    private int sprite;

    @Column(name = "posX")
    private int posX;

    @Column(name = "posY")
    private int posY;

    @Column(name = "mapX")
    private int mapX;

    @Column(name = "mapY")
    private int mapY;

    @Column(name = "gender")
    private int gender;

    @Column(name = "skin")
    private int skin;

    @Column(name = "hairStyle")
    private int hairStyle;

    @Column(name = "hairColor")
    private int hairColor;

    @Enumerated(EnumType.STRING)
    @Column(name = "playerStatus")
    private PlayerStatusEnum playerStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "playerType")
    private PlayerTypeEnum playerType;

    @Enumerated(EnumType.STRING)
    @Column(name = "language")
    private LanguageEnum language;

    @Column(name = "isMuted")
    private boolean isMuted;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clothingId", nullable = true)
    private Item clothing;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hatId", nullable = true)
    private Item hat;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "vehicleId", nullable = true)
    private Vehicle vehicle;

    @Column(name = "lastPlayTime")
    private DateTime lastPlayTime;

}
