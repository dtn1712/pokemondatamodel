package com.rockitgaming.pokemon.data.model.entity.pokemon;


public enum HabitatEnum {
    Cave, Forest, Grassland, Mountain, Rare, Rough_Terrain, Sea, Urban, Waters_Edge

}
