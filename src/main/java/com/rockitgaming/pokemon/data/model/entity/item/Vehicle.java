package com.rockitgaming.pokemon.data.model.entity.item;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = TableName.VEHICLE_TABLE)
public class Vehicle extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "speed")
    private int speed;
}
