package com.rockitgaming.pokemon.data.model.entity.item;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = TableName.ITEM_CATEGORY_TABLE)
public class ItemCategory extends BaseEntity {

    @Column(name = "key", nullable = false)
    private String key;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "pocketId", nullable = false)
    private ItemPocket pocket;
}
