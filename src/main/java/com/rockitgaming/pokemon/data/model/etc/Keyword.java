package com.rockitgaming.pokemon.data.model.etc;

/**
 * Created by dangn on 7/24/16.
 */
public enum Keyword {

    ALL, AND, OR
}
