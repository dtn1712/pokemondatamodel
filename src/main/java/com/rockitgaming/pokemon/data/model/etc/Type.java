package com.rockitgaming.pokemon.data.model.etc;


public enum Type {
    Normal, Fighting, Flying, Poison, Ground, Rock, Bug, Ghost, Steel, Fire, Water, Grass, Eletric, Psychic, Ice, Dragon, Dark, Fairy, Unknown, Shadow

}
