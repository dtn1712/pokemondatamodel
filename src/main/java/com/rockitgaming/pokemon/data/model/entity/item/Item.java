package com.rockitgaming.pokemon.data.model.entity.item;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = TableName.ITEM_TABLE)
public class Item extends BaseEntity{

    @Column(name = "name")
    private String name;

    @Column(name = "cost")
    private Long cost;

    @Column(name = "effectDescription", columnDefinition = "TEXT")
    private String effectDescription;

    @ManyToOne
    @JoinColumn(name = "categoryId", nullable = false)
    private ItemCategory category;

}
