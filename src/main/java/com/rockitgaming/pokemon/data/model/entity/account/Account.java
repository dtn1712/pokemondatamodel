
package com.rockitgaming.pokemon.data.model.entity.account;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.BaseEntity;
import com.rockitgaming.pokemon.data.model.entity.player.Player;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = TableName.ACCOUNT_TABLE)
public class Account extends BaseEntity {

    public static final String IS_CURRENT_LOGIN_CACHE_KEY = "isCurrentLogin";
    public static final String GET_CURRENT_LOGIN_ACCOUNT_CACHE_KEY = "getCurrentLoginAccount";

    public static final String CURRENT_LOGIN_PATTERN_CACHE_KEY = "currentLogin";

    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "phone", unique = true)
    private String phone;

    @Column(name = "password")
    private String password;

    @Column(name = "createdAt", nullable = false, updatable = false, insertable = false, columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP")
    private DateTime createdAt;

    @Column(name = "lastSuccessLoginTime")
    private DateTime lastSuccessLoginTime;

    @Enumerated(EnumType.STRING)
    @Column(name="accountType")
    private AccountTypeEnum accountType = AccountTypeEnum.NORMAL;

    @Enumerated(EnumType.STRING)
    private AccountStatusEnum accountStatus = AccountStatusEnum.ACTIVE;

    @OneToOne
    @JoinColumn(name = "currentPlayerId")
    private Player currentPlayer;
}
