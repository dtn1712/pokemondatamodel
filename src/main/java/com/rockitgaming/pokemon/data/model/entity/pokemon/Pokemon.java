package com.rockitgaming.pokemon.data.model.entity.pokemon;


import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = TableName.POKEMON_TABLE)
public class Pokemon extends BaseEntity{
}
