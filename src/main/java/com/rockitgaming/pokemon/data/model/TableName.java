package com.rockitgaming.pokemon.data.model;


public class TableName {

    public static final String ACCOUNT_TABLE = "account";
    public static final String ACCOUNT_LOCK_TABLE = "accountlock";
    public static final String ACCOUNT_ATTEMPT_LOGIN_TABLE = "accountattemptlogin";

    public static final String ITEM_TABLE = "item";
    public static final String ITEM_CATEGORY_TABLE = "itemcategory";
    public static final String ITEM_POCKET_TABLE = "itempocket";
    public static final String VEHICLE_TABLE = "vehicle";

    public static final String FRIEND_TABLE = "friend";
    public static final String FRIEND_REQUEST_TABLE = "friendrequest";
    public static final String PLAYER_BAG_ITEM_TABLE = "playerbagitem";
    public static final String PLAYER_POKEDEX_TABLE = "playerpokedex";
    public static final String PLAYER_PARTY_TABLE = "playerparty";
    public static final String PLAYER_TABLE = "player";

    public static final String POKEMON_TABLE = "pokemon";
    public static final String POKEMON_SPECIES_TABLE = "pokemonspecies";
    public static final String POKEMON_TYPE_TABLE = "pokemontype";
}
