package com.rockitgaming.pokemon.data.model.repository.player;

import com.rockitgaming.pokemon.data.model.entity.player.Player;
import com.rockitgaming.pokemon.data.model.entity.player.PlayerBagItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayerBagItemRepository extends JpaRepository<PlayerBagItem, Long> {

    List<PlayerBagItem> findByPlayer(Player player);
}
