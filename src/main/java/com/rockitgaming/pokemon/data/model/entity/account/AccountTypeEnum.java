package com.rockitgaming.pokemon.data.model.entity.account;

public enum AccountTypeEnum {

    NORMAL,
    ADMIN
}
