package com.rockitgaming.pokemon.data.model.repository.player;

import com.rockitgaming.pokemon.data.model.entity.player.FriendRequest;
import com.rockitgaming.pokemon.data.model.entity.player.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendRequestRepository extends JpaRepository<FriendRequest, Long> {

    List<FriendRequest> findByFromPlayer(Player fromPlayer);

    List<FriendRequest> findByToPlayer(Player toPlayer);
}
