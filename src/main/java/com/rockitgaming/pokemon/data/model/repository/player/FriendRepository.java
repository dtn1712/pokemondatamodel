package com.rockitgaming.pokemon.data.model.repository.player;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.player.Friend;
import com.rockitgaming.pokemon.data.model.entity.player.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendRepository extends JpaRepository<Friend, Long> {

    @Query(value = "SELECT * FROM " + TableName.PLAYER_TABLE +
            " WHERE EXISTS (SELECT 1 FROM " + TableName.FRIEND_TABLE + " WHERE " +
            " (" + TableName.PLAYER_TABLE + ".id <> " + TableName.FRIEND_TABLE + ".player1Id AND " + TableName.FRIEND_TABLE + ".player1Id = :playerId) OR " +
            " (" + TableName.PLAYER_TABLE + ".id <> " + TableName.FRIEND_TABLE + ".player2Id AND " + TableName.FRIEND_TABLE + ".player2Id = :playerId))",
            nativeQuery = true)
    List<Player> getPlayerFriends(@Param("playerId") String playerId);
}
